#include "Vector.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

//A
// constructor with initialization line
Vector::Vector(int n)
{
	if (n < MIN_SIZE)
		n = MIN_SIZE;
	// allocates memory and assigns elements
	this->_capacity = n;
	this->_resizeFactor = n;
	this->_size = 0;
	this->_elements = new int[n];
	for (int i = 0; i < n; i++)
	{
		this->_elements[i] = EMPTY_ELEMENT;
	}
}
// delete all data about the vector
Vector::~Vector()
{
	if (this->_elements)
	{
		delete[] this->_elements;
	}
	this->_elements = nullptr;
	this->_size = 0;
	this->_capacity = 0;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

// function check if vector is empty
// input - none
// output - true if empty false if not
bool Vector::empty() const
{
	if (this->_size == 0)
		return true;
	else
		return false;
}
// function prints vector's data
void Vector::print() const
{
	cout << "size: " << this->_size << endl
		 << "resize factor: " << this->_resizeFactor << endl
		 << "capacity: " << this->_capacity << endl
		 << "*** Elements ***" << endl;
	for (int i = 0; i < this->_capacity; i++)
		cout << "element " << i <<" -> "<< this->_elements[i] << endl;
}

//B
//Modifiers
void Vector::push_back(const int & val)
{
	int* tempArr = new int[this->_size];
	// if the vector is full
	if (this->_size == this->_capacity)
	{
		for (int i = 0; i < this->_size; i++)
			tempArr[i] = this->_elements[i];
		delete[] this->_elements; 
		this->_elements = new int[this->_capacity + this->_resizeFactor];
		for (int i = 0; i < this->_size; i++)
			this->_elements[i] = tempArr[i];
		this->_elements[this->_size] = val;
		this->_capacity += this->_resizeFactor;
		this->_size++;
		for (int i = this->_size; i < this->_capacity; i++)
			this->_elements[i] = EMPTY_ELEMENT;
	}
	else
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	delete tempArr;
}

int Vector::pop_back()
{
	int poppedArg;
	if (this->empty())
	{
		cout << "Error: pop from empty vector" << endl;
		return -9999;
	}
	else
	{
		this->_size--;
		poppedArg = this->_elements[this->_size];
		this->_elements[this->_size] = EMPTY_ELEMENT;
		cout << "Element was popped!" << endl;
		return poppedArg;
	}
}

void Vector::reserve(int n)
{
	int* tempArr;
	int resize;
	if (this->_capacity < n)
	{
		(n - this->_capacity) % this->_resizeFactor == 0 ? resize = (n - this->_capacity) / this->_resizeFactor : resize = ((n - this->_capacity) / this->_resizeFactor) + 1;
		tempArr = new int[this->_capacity + (this->_resizeFactor * resize)];
		for (int i = 0; i < this->_size; i++)
			tempArr[i] = this->_elements[i];
		delete[] this->_elements;
		this->_elements = new int[this->_capacity +(this->_resizeFactor * resize)];
		for (int i = 0; i < this->_size; i++)
			this->_elements[i] = tempArr[i];
		this->_capacity += this->_resizeFactor * resize;
		for (int i = this->_size; i < this->_capacity; i++)
			this->_elements[i] = EMPTY_ELEMENT;
		delete tempArr;
	}
	// else the capacity is bigger and shouldn't change
}

void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		this->reserve(n);
		this->_size = n;
	}
}
// function copy new value to all indexes
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
		this->_elements[i] = val;
}

void Vector::resize(int n, const int & val)
{
	int start = _size;
	this->resize(n);
	for (int i = start; i < _size; ++i)
	{
		this->_elements[i] = val;
	}
}

//C
//The big three (d'tor is above)
Vector::Vector(const Vector & other)
{
	this->_size = other.size();
	this->_capacity = other.capacity();
	this->_resizeFactor = other.resizeFactor();
	// creating new array to copy elements
	this->_elements = new int[other.capacity()];
	for (int i = 0; i < this->_capacity; i++)
	{
		_elements[i] = other._elements[i];
	}
}

Vector & Vector::operator=(const Vector & other)
{
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}

	delete[] this->_elements; // release old memory

	// shallow copy fields
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_capacity = other._capacity;

	// deep copy dynamic fields (pointers/arrays)
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_capacity; i++)
	{
		// copies cell by cell
		this->_elements[i] = other._elements[i];
	}
	return *this;
}

//D
//Element Access
int & Vector::operator[](int n) const
{
	if (n >= this->_size || n < 0)// out of range
	{
		cout << "error. index out of range" << endl;
		n = 0;// returning first argument
	}
	return this->_elements[n];
}
