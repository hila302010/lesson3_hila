#include "Vector.h"
#include <iostream> // std::cout, std::endl

using std::cout;
using std::endl;

int main()
{
	// initiates a vector object
	Vector A(3);
	cout << A.empty() << endl;
	cout << A.pop_back() << endl;
	A.push_back(8);
	A.push_back(4);
	A.print();
	cout<<A.pop_back()<<endl;
	A.print();
	A.reserve(5);
	A.print();
	A.assign(2);
	

	system("pause");
	return 0;
}

